define(function(require) {
     var v1 = require('my/atme-list');
     var v2 = require('my/comment-detail');
     var v3 = require('my/comment-list');
     var v4 = require('my/critic-list');
     var v5 = require('my/index');
     var v6 = require('my/my-list');
     var v7 = require('my/myself-list');
     var v8 = require('my/comment-transpond');
     return {
         'atme-list': v1,
         'comment-detail': v2,
         'comment-list': v3,
         'critic-list': v4,
         'index': v5,
         'my-list': v6,
         'myself-list': v7,
         'comment-transpond': v8
      };
});
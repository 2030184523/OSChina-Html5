define(function(require) {
     var v1 = require('template/blog-common-comment.json');
     var v2 = require('template/collect');
     var v3 = require('template/common-comment');
     var v4 = require('template/detail');
     var v5 = require('template/message-list-json');
     var v6 = require('template/my-list-json');
     var v7 = require('template/news-blog-detail');
     var v8 = require('template/news-blog');
     var v9 = require('template/news-detail-json');
     var v10 = require('template/news-list-json');
     var v11 = require('template/news-recommend');
     var v12 = require('template/page');
     var v13 = require('template/question-list-json');
     var v14 = require('template/search');
     var v15 = require('template/tweet-list-json');
     return {
         'blog-common-comment.json': v1,
         'collect': v2,
         'common-comment': v3,
         'detail': v4,
         'message-list-json': v5,
         'my-list-json': v6,
         'news-blog-detail': v7,
         'news-blog': v8,
         'news-detail-json': v9,
         'news-list-json': v10,
         'news-recommend': v11,
         'page': v12,
         'question-list-json': v13,
         'search': v14,
         'tweet-list-json': v15
      };
});